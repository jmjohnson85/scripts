#NoEnv                       ; Recommended for performance and compatibility with future AutoHotkey releases
#SingleInstance IGNORE       ; Attempts to launch an already-running script are ignored
#NoTrayIcon                  ; Prevent the showing of a tray icon for this script
SetWorkingDir, %A_ScriptDir% ; Makes the script unconditionally use its own folder as its working directory
SetTitleMatchMode, 2         ; A window's title can contain WinTitle anywhere inside it to be a match

showSqlGridView(server, database, username, password, query) {
    command =
    (
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

        if (-Not (Get-Module -ListAvailable -Name SimplySql)) {
            Find-PackageProvider -Name NuGet -ForceBootstrap -IncludeDependencies | Out-Null
            Install-Module -Name SimplySql -Scope CurrentUser -Force
        }

        Import-Module -Name SimplySql -Scope Local -Force

        Open-MySqlConnection -Server '%server%' -Database '%database%' -UserName '%username%' -Password '%password%' -WarningAction SilentlyContinue
        $results = Invoke-SqlQuery -Query '%query%' -AsDataTable

        Close-SqlConnection
        Remove-Module -Name SimplySql -Force

        $results | Out-GridView -Wait
    )
    Run, PowerShell.exe -ExecutionPolicy Bypass -WindowStyle Hidden -NonInteractive -NoProfile -Command &{%command%},, Min
}

showMarqueeProgressBar(xPos, yPos, width, height)
{
    ; Taken from https://autohotkey.com/board/topic/24700-indeterminate-progressbar/
    PBM_SETMARQUEE := 0x40a ;wm_user + 10
    Gui, Add, Progress, x10 y10 w300 h15 HwndpHwnd1 -Smooth +0x8  ;0x8 =  PBS_MARQUEE
    Gui, Show
    PostMessage, PBM_SETMARQUEE, 1, 38, , ahk_id %pHwnd1% ;_wParam-Bool (1 = start , 0 = stop),lParam-Time (ms) between animation updates.
    return
}

server := "codext.davenportiowa.com"
query := "SELECT * FROM submissions"
database := "dream"
username := "letty.goslowsky"
password := "PAne6a1267w65Obopax17oXiJofAK7"

showMarqueeProgressBar(10, 10, 150, 20)
showSqlGridView(server, database, username, password, query)

WinWait, Out-GridView

ExitApp
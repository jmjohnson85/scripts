# https://stackoverflow.com/questions/71788518/git-rename-all-default-branchesimport gitlab

gl = gitlab.Gitlab("https://gitlab.example.com", private_token="your token")

# add your project IDs here
all_projects = [gl.projects.get(1234), gl.projects.get(4567), ...]

# or list all projects programatically: (careful if using gitlab.com!)
# all_projects = list(gl.projects.list(as_list=False))


def get_or_create_main_branch(project):
    try:
        b = project.branches.get("main")
        print("main branch already exists")
        return b
    except gitlab.exceptions.GitlabError:
        print("Creating main branch from ref:", project.default_branch)
        project.branches.create({"branch": "main", "ref": project.default_branch})
        print("Creating protection rule for main branch")
        # Edit access levels per your needs
        project.protectedbranches.create(
            {
                "name": "main",
                "merge_access_level": gitlab.const.DEVELOPER_ACCESS,
                "push_access_level": gitlab.const.MAINTAINER_ACCESS,
            }
        )
        return project.branches.get("main")


def fix_project(project):
    main_branch = get_or_create_main_branch(project)
    print("setting default branch to main...", end="")
    old_default_branch = project.branches.get(project.default_branch)
    project.default_branch = "main"
    project.save()
    print("Done!")
    print("deleting original default branch", old_default_branch.name, "...", end="")
    old_default_branch.delete()
    print("done!")


for project in all_projects:
    print("-" * 20)
    print("Checking", project.path_with_namespace, "for default branch")
    if project.default_branch != "main":
        print(
            f'Default branch is "{project.default_branch}", not "main". Attempting Fix...'
        )

        try:
            fix_project(project)
        except gitlab.exceptions.GitlabError as e:
            # sometimes bare repos without any commits will error out
            print("FATAL. Failure getting or creating main branch", e)
            print("Skipping")
            continue  # move on to next project in the loop
        print("successfully fixed", project.path_with_namespace)
    else:
        print('Default branch is already "main", skipping')
        continue
# Set-PowerCLIConfiguration -Scope Session -ParticipateInCeip $false -DefaultVIServerMode Single -InvalidCertificateAction Ignore -Confirm:$false

# $User = "jeremy.m.johnson"
# $Password = "[super-secret-password]"
# $Server1Address = "vcs"
# $Server2Address = "srv-vcs02"

# Connect-VIServer -Server $Server1Address -User $User -Password $Password
# $vcs_guests = Get-VM
# $vcs_macs = $vcs_guests | ForEach-Object {$_.Guest | Select-Object -ExpandProperty Nics | Select-Object -Property MacAddress}

# Connect-VIServer -Server $Server2Address -User $User -Password $Password
# $vcs02_guests = Get-VM
# $vcs02_macs = $vcs02_guests | ForEach-Object {$_.Guest | Select-Object -ExpandProperty Nics | Select-Object -Property MacAddress}

# Compare-Object -ReferenceObject $vcs_macs -DifferenceObject $vcs02_macs -ExcludeDifferent



<#   
.SYNOPSIS
Function that compares all VMs in two vCenter servers based on a specified property
    
.DESCRIPTION 
Uses VMWare PowerCLI to connect to both vCenter servers, gets a list of all VMs then performs the compare

.NOTES
This function assumes that the login credentials will be the same for both vCenter servers!

.PARAMETER User
[string] The username to use when connecting to both vCenter servers

.PARAMETER Password
[string] The password to use when connecting to both vCenter servers

.PARAMETER Server1Address
[string] The hostname or IP address of the first vCenter server to connect to

.PARAMETER Server2Address
[string] The hostname or IP address of the second vCenter server to connect to

.PARAMETER CompareProperty

.OUTPUTS
A PSCustomObject containing the result(s) of the comparison

.NOTES   
Name: Compare-vCenterGuests.ps1
Author: Jeremy Johnson
Date Created: 3-31-2021
Date Updated: 3-31-2021
Version: 1.0.0

.LINK
https://www.jmjohnson85.com

.EXAMPLE
    PS > . .\Compare-vCenterVMs.ps1

.EXAMPLE
    PS > Compare-vCenterVMs -User "[username]" -Password "[password]" -Server1Address "[vcenter1]" -Server2Address "[vcenter2]" -CompareProperty "[property]"

.EXAMPLE
    PS > Compare-vCenterVMs -User jeremy.m.johnson -Password [super-secret-password] -Server1Address vcs -Server2Addres srv-vcs02 -CompareProperty MacAddress

.EXAMPLE
    PS > Compare-vCenterVMs -u jeremy.m.johnson -p [pass] -s1 vcs -s2 srv-vcs02 -c MacAddress
#>

function Compare-vCenterVMs {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]
        [Alias('u')]
        [string] $User,

        [Parameter(Mandatory=$true)]
        [Alias('p')]
        [string] $Password,

        [Parameter(Mandatory=$true)]
        [Alias('s1')]
        [string] $Server1Address,

        [Parameter(Mandatory=$true)]
        [Alias('s2')]
        [string] $Server2Address,

        [Parameter(Mandatory=$true)]
        [Alias('c')]
        [string] $CompareProperty
    )

    begin {
        Set-PowerCLIConfiguration -Scope Session -ParticipateInCeip $false -DefaultVIServerMode Single -InvalidCertificateAction Ignore -Confirm:$false | Out-Null
    }
    
    process {
        function Get-CompareResults {
            param (
                [string] $ServerAddress
            )
            Connect-VIServer -Server $ServerAddress -User $User -Password $Password
            $results = Get-VM | ForEach-Object {
                $_.Guest
                | Select-Object -ExpandProperty Nics
                | Select-Object -Property "$CompareProperty"
            }
            return $results
        }

        $s1Results = Get-CompareResults -ServerAddress $Server1Address
        $s2Results = Get-CompareResults -ServerAddress $Server2Address

        return $result
    }
    
    end {

    }
}
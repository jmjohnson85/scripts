function Get-EnvironmentVariable {
	[CmdletBinding()]
	param (
	    [Parameter(Mandatory)]
        [Alias('n')]
        [string] $Name
	)
	Get-Content -Path "env:\${Name}"
}

function Set-EnvironmentVariable {
	[CmdletBinding()]
	param (
		[Parameter(Mandatory)]
		[string] $Name,
		
		[Parameter(Mandatory)]
		[string] $Value,
		
		[Parameter(Mandatory=$false)]
		[ValidateSet('Process', 'User', 'Machine')]
		[string] $Scope = 'User'
	)
	# set environment variable for current session
	Set-Item -Path "env:${Name}" -Value $Value
	
	# set environment variable for specific scope
	[System.Environment]::SetEnvironmentVariable($Name, $Value, $Scope)
}
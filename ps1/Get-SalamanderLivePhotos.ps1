<#   
.SYNOPSIS
Function that downloads all ID photos from 'app.salamanderlive.com'
    
.DESCRIPTION 
Signs into SalamanderLive using the supplied credentials, gets a list of all 'People' in the system, then downloads the photo for each person

.PARAMETER UserName
[string] The username to login with

.PARAMETER Password
[string] The password to login with

.PARAMETER OutPath
[string] Optional path of where to store all downloaded photos (defaults to current location)

.INPUTS
None. You cannot pipe objects to Get-SalamanderLivePhotos

.OUTPUTS
[int] The final count of all downloaded photos

.NOTES   
Name: Get-SalamanderLivePhotos.ps1
Author: Jeremy Johnson
Date Created: 7-7-2021
Date Updated: 8-17-2021
Version: 1.0.2

.EXAMPLE
    PS > . .\Get-SalamanderLivePhotos.ps1

.EXAMPLE
    PS > Get-SalamanderLivePhotos -UserName '[username]' -Password '[password]' -OutPath '[output\path\]'

.EXAMPLE
    PS > Get-SalamanderLivePhotos -u '[user]' -p '[pass]' -o '[path\]'
#>

#Requires -Version 7

$storedCount = 0

function Get-SalamanderLivePhotos {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]
        [Alias('u')]
        [string] $UserName,

        [Parameter(Mandatory=$true)]
        [Alias('p')]
        [securestring] $Password,

        [Parameter(Mandatory=$false)]
        [Alias('o')]
        [string] $OutPath = $ExecutionContext.SessionState.Path.CurrentFileSystemLocation.Path
    )

    begin {
        [System.Net.ServicePointManager]::ServerCertificateValidationCallback = {
            return $true # Disable HTTPS certificate validation
        }

        function New-LoginBody {
            param (
                [string] $Username,
                [securestring] $Password
            )
            [string] $Password = ConvertFrom-SecureString -SecureString $Password -AsPlainText
            return "{
                ""UserName"":""${UserName}"",
                ""Password"":""${Password}""
            }"
        }

        function New-PeopleListBody {
            param (
                [string] $RecordsPerPage
            )
            return "{
                ""TimeZoneId"":""Central Standard Time"",
                ""Item"":{
                    ""Page"":1,
                    ""RecordsPerPage"":""${RecordsPerPage}"",
                    ""Sort"":[],
                    ""Filter"":[],
                    ""Columns"":[
                        ""LastName"",
                        ""FirstName"",
                        ""IdentityCode"",
                        ""Rank"",
                        ""OrganizationName""
                        ]
                    },
                    ""Save"":false
                }"
        }

        function Convert-ResponseContent {
            param (
                [object] $Content
            )
            if ($Content -isnot [array]) {
                $content = ConvertFrom-Json -InputObject $Content
                return $content.Data
            } else {
                return $Content
            }
        }

        function Get-ResponseData {
            param (
                [string] $Path,
                [string] $Method,
                [string] $Body = '',
                [string] $Token = ''
            )
            try {
                $response = Invoke-WebRequest `
                    -ContentType 'application/json' `
                    -Uri "https://app.salamanderlive.com/resourcemgrsvc/${Path}" `
                    -Method $Method `
                    -Body $Body `
                    -Headers @{
                        'Host' = 'app.salamanderlive.com';
                        'Authorization' = "Bearer $Token"
                    };
                $data = Convert-ResponseContent -Content $response.Content
                return $data
            } catch {
                $message = $_.Exception.Response.ReasonPhrase ?? $_.Exception.Message
                Write-Warning -Message "[${Path}] ${message}"
            }
        }

        function Get-Token {
            param (
                [string] $UserName,
                [securestring] $Password
            )
            $loginBody = New-LoginBody -Username $UserName -Password $Password
            $loginData = Get-ResponseData -Path 'User/Login' -Method 'Post' -Body $loginBody
            $token = $loginData.TokenID
            return $token
        }

        function Get-People {
            param (
                [string] $Token
            )
            $peopleBody = New-PeopleListBody -RecordsPerPage 1000
            $people = Get-ResponseData -Path 'Responder/List' -Method 'Post' -Body $peopleBody -Token $Token
            return $people
        }

        function Get-PhotoBytes {
            param (
                [object] $Person,
                [string] $Token
            )
            $id = $Person.PK
            $path = "Responder/Image/${id}"
            $bytes = Get-ResponseData -Path $path -Method 'Get' -Token $Token
            return $bytes
        }

        function New-PhotoFileName {
            param (
                [object] $Person
            )
            $firstName = $Person.FirstName
            $lastName = $Person.LastName
            $idCode = $Person.IdentityCode
            $fileName = "${lastName},${firstName}CDB${idCode}.jpg"
            return $fileName
        }

        function Out-JpegFile {
            param (
                [string] $Path,
                [byte[]] $Bytes
            )
            if ($null -ne $Bytes) {
                try {
                    Write-Host -ForegroundColor Green -Object "STORING: [${Path}]"
                    [IO.File]::WriteAllBytes($Path, $Bytes)
                    $script:storedCount++
                } catch {
                    $message = $_.Exception.Message
                    Write-Warning -Message "[${Path}] ${message}"
                }
            }
        }
    }

    process {
        $token = Get-Token -UserName $UserName -Password $Password
        $people = Get-People -Token $token
        foreach ($person in $people) {
            $fileName = New-PhotoFileName -Person $person
            $path = Join-Path -Path $OutPath -ChildPath $fileName
            $photoBytes = Get-PhotoBytes -Person $person -Token $token
            Out-JpegFile -Path $path -Bytes $photoBytes
        }
    }

    end {
        Write-Host -ForegroundColor White -Object "FINISHED: [${storedCount}] photos were stored`n"
    }
}

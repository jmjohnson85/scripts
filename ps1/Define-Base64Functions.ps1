function ConvertFrom-Base64 {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [Alias('v')]
        [string] $Value
    )
    $decoded = [System.Convert]::FromBase64String($Value)
    $plain = [System.Text.Encoding]::ASCII.GetString($decoded)
    return $plain
}

function ConvertTo-Base64 {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [Alias('v')]
        [string] $Value
    )
    $bytes = [Text.Encoding]::ASCII.GetBytes($Value)
    $base64 = [Convert]::ToBase64String($bytes)
    return $base64
}
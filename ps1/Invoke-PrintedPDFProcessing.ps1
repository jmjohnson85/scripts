. C:\IT\PowerShell\Write-Log.ps1;
. C:\IT\PowerShell\Move-PrintedPDF.ps1;
. C:\IT\PowerShell\Send-PrintedPDFEmail.ps1;

$path = 'C:\IT\Temp\PrintPDFUnattended-Output.pdf'
$dest = 'C:\U2\PRINTED'

Move-PrintedPDF -Path $path -Destination $dest |
    Send-PrintedPDFEmail |
    Write-Log -Path "$dest\!sent-pdfs.log"
<#   
.SYNOPSIS
Function that creates a new Elastic Build Runner (EBR) instance
    
.DESCRIPTION 
Uses curl.exe to invoke the Ansible Automation Platform (AAP) API to launch the EBR provisioning template

.PARAMETER Name
[string] The desired name to give the new EBR instance [ebr_name]

.PARAMETER Email
[string] The email address of the new EBR instance owner [ebr_email]

.PARAMETER BusinessUnit
[string] The Busines Unit (BU) who the new EBR instance is being created for [business_unit]

.PARAMETER GitLabToken
[string] The registration token for connecting the GitLab Agent [gitlab_token]

.PARAMETER GitLabKasAddress
[string] The URL of the GitLab Kubernetes Agent Server (KAS) [gitlab_kasAddress]

.PARAMETER Token
[string] The access token to use when invoking the Ansible Automation Platform (AAP) API

.PARAMETER Url
[string] (optional) The URL of the Ansible Automation Platform (AAP) API

.PARAMETER OutputPath
[string] (optional) The local path of where to save the generated kubeconfig artifact

.PARAMETER UseNonProd
[switch] (optional) Use the non-prod Ansible Workflow Template (de-ebr-dev)

.NOTES
- Once PowerShell 7.4.0 stable is released this can be updated to use Invoke-WebRequest instead of curl.exe.
- This version ^^^ will introduce a new flag -PreserveHttpMethodOnRedirect with is equivalent to --post301
- See: https://github.com/PowerShell/PowerShell/releases/tag/v7.4.0-preview.2

.NOTES
- Name: New-EbrInstance
- Author: Jeremy Johnson
- Date Created: 7-13-2023
- Date Updated: 8-1-2023
- Version: 1.0.10

.EXAMPLE
    PS > . .\New-EbrInstance.ps1

.EXAMPLE
    PS > New-EbrInstance

.EXAMPLE
    PS > $gitLabToken = 'gLLmgbGq-MriE9pQQzsZpxtKcTdJvDT8PcAdKV7JQLngvESgxw'
    PS > $gitLabKasAddress = 'wss://rclinux-git.rockwellcollins.com/-/kubernetes-agent/'
    PS > $token = 'GegACJIIZbn1qTXlMamp2WuB39H3Lz'
    ...
    PS > 
    PS > New-EbrInstance -GitLabToken $gitLabToken -GitLabKasAddress $gitLabKasAddress -Token $token ...
#>

function New-EbrInstance {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$false)]
        [Alias('n')]
        [string] $Name = 'example-ebr-instance',

        [Parameter(Mandatory=$false)]
        [Alias('e')]
        [string] $Email = 'admin@example.ebr',

        [Parameter(Mandatory=$false)]
        [Alias('b')]
        [string] $BusinessUnit = 'dplc-ds3',

        [Parameter(Mandatory=$false)]
        [string] $GitLabToken = '6Q_8ro8M-BaeMrz_57_FQ3nt6ZWH3RzWJty_EsFBMEtQdj1j2Q',

        [Parameter(Mandatory=$false)]
        [string] $GitLabKasAddress = 'ws://dplc-tc-gitlab-ea-kas.ds3-dplc-ea.svc.cluster.local:8150/-/kubernetes-agent/',

        [Parameter(Mandatory=$false)]
        [Alias('t')]
        [string] $Token = 'GegACJIIZbn1qTXlMamp2WuB39H3Lz',

        [Parameter(Mandatory=$false)]
        [Alias('u')]
        [string] $Url = 'https://ansiblev2.collins.com',

        [Parameter(Mandatory=$false)]
        [Alias('o')]
        [string] $OutputPath = "$env:USERPROFILE\Desktop",

        [Parameter(Mandatory=$false)]
        [Alias('d')]
        [switch] $UseNonProd
    )

    begin {
        function Test-ApiToken {
            $code = curl.exe -k `
                --silent `
                --location "${Url}/api/v2/me" `
                --header "Authorization: Bearer ${Token}" `
                --output "/dev/null" `
                --write-out "%{http_code}"
            if ($code -ne 200) {
                throw "Authentication failed. API response code was ${code}"
            }
        }

        function Resolve-Ternary {
            # necessary to support powershell versions < 7.0
            param (
                [bool] $Condition,
                [object] $IfTrue,
                [object] $Else
            )
            if ($Condition) {
                return $IfTrue
            } else {
                return $Else
            }
        }

        function Get-WorkflowTemplateId {
            $message = "Using Workflow Template:"
            if ($UseNonProd) {
                $workflowTemplateId = 51
                Write-Warning -Message "${message} ${workflowTemplateId} [Non-Prod]"
            } else {
                $workflowTemplateId = 66
                Write-Host -Object "${message} ${workflowTemplateId}" -ForegroundColor Green
            }
            return $workflowTemplateId
        }

        function New-WorkflowJobBody {
            return @{
                extra_vars = @{
                    ebr_name = $Name;
                    ebr_email = $Email;
                    business_unit = $BusinessUnit;
                    gitlab_kasAddress = $GitLabKasAddress;
                    gitlab_token = $GitLabToken;
                }
            }
        }

        function Start-WorkflowJob {
            $body = New-WorkflowJobBody | ConvertTo-Json -Compress
            $workflowTemplateId = Get-WorkflowTemplateId
            $json = $body | curl.exe -k `
                --silent `
                --location `
                --post301 "${Url}/api/v2/workflow_job_templates/${workflowTemplateId}/launch/" `
                --header "Content-Type: application/json" `
                --header "Authorization: Bearer ${Token}" `
                --data "@-"
            $workflowJob = ConvertFrom-Json -InputObject $json
            return $workflowJob
        }

        function Invoke-GetRequest {
            param (
                [string] $Path
            )
            $json = curl.exe -k `
                --silent `
                --request GET `
                --location "${Url}${Path}" `
                --header "Authorization: Bearer ${Token}"
            $response = ConvertFrom-Json -InputObject $json
            return $response
        }

        function Wait-WorkflowJob {
            param (
                [object] $WorkflowJob,
                [int] $SleepSeconds = 2
            )
            $id = $WorkflowJob.workflow_job
            $path = $WorkflowJob.url
            $activity = "Awaiting Workflow Job ${id}"
            do {
                $WorkflowJob = Invoke-GetRequest -Path $path
                $percentComplete = Resolve-Ternary -Condition ($percentComplete -ge 100) -IfTrue 2 -Else ($percentComplete + 2)
                Write-Progress -Activity $activity -Status $WorkflowJob.status -PercentComplete $percentComplete
                Start-Sleep -Seconds $SleepSeconds
            } until ($WorkflowJob.status -notin @('running','pending'))
            Write-Progress -Activity $activity -Completed
            return $WorkflowJob
        }

        function Get-WorkflowNode {
            param (
                [object] $WorkflowJob
            )
            $path = $WorkflowJob.related.workflow_nodes
            $workflowNodes = Invoke-GetRequest -Path $path
            $templateId = Resolve-Ternary -Condition $UseNonProd -IfTrue 39 -Else 38
            $property = 'unified_job_template'
            $node = $workflowNodes.results | Where-Object -Property $property -eq $templateId
            return $node
        }

        function Out-JsonFile {
            param (
                [string] $Base64Value,
                [string] $FilePath
            )
            $bytes = [System.Convert]::FromBase64String($Base64Value)
            $json = [System.Text.Encoding]::UTF8.GetString($bytes)
            $prettyJson = ConvertFrom-Json -InputObject $json | ConvertTo-Json -Depth 10
            Out-File -InputObject $prettyJson -FilePath $FilePath
        }

        function Get-JobArtifacts {
            param (
                [object] $WorkflowNode
            )
            $path = $WorkflowNode.related.job
            $job = Invoke-GetRequest -Path $path
            return $job.artifacts
        }

        function Out-Kubeconfig {
            param (
                [object] $JobArtifacts
            )
            $ebrKey = $JobArtifacts.ebr_key
            $kubeconfigPath = "${OutputPath}\config-${ebrKey}.json"
            $kubeconfigB64 = $JobArtifacts.k8s_generated_kubeconfig
            Out-JsonFile -Base64Value $kubeconfigB64 -FilePath $kubeconfigPath
            Write-Host -Object "Kubeconfig file saved at: ${kubeconfigPath}"
        }
    }

    process {
        Test-ApiToken
        $workflowJob = Start-WorkflowJob
        $script:workflowJob = Wait-WorkflowJob -WorkflowJob $workflowJob
        $workflowNode = Get-WorkflowNode -WorkflowJob $script:workflowJob

        if ($script:workflowJob.status -eq 'successful') {
            $jobArtifacts = Get-JobArtifacts -WorkflowNode $workflowNode
            Out-Kubeconfig -JobArtifacts $jobArtifacts
        }
    }

    end {
        $id = $script:workflowJob.id
        $status = $script:workflowJob.status
        Write-Host -Object "Workflow Job ${id} status was: ${status}`n"
    }
}

#New-EbrInstance # leave for instant debugging in VSCode
<#   
.SYNOPSIS
Function that clones all git repos in Azure Devops when given an OrganizationUrl
    
.DESCRIPTION 
Uses Azure CLI + Azure DevOps Extension to connect to Azure DevOps, retrieves a list of repo URLs, then runs 'git clone' for each one

.PARAMETER OrganizationUrl
[string] The URL of the organization hosted on Azure DevOps to be cloned

.INPUTS
None. You cannot pipe objects to Clone-AzureDevOps

.OUTPUTS
Only the stdout produced from executing 'git clone'

.NOTES   
Name: Clone-AzureDevOps.ps1
Original Author: Simon Wahlin
Modified By: Jeremy Johnson
Date Created: 4-8-2021
Date Updated: 9-20-2021
Version: 1.0.2

.LINK
https://blog.simonw.se/cloning-all-repositories-from-azure-devops-using-azure-cli/

.EXAMPLE
    PS > . .\Clone-AzureDevOps.ps1

.EXAMPLE
    PS > Clone-AzureDevOps -OrganizationUrl [organization-url]

.EXAMPLE
    PS > Clone-AzureDevOps -o [org-url]

.EXAMPLE
    PS > Clone-AzureDevOps -o 'https://dev.azure.com/davenportiowa'
#>

function Clone-AzureDevOps {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]
        [Alias('o')]
        [string] $OrganizationUrl
    )

    begin {
        function Get-ProjectNames {
            return az devops project list --organization $OrganizationUrl --query 'value[].name' -o tsv
        }

        function Clone-Projects {
            param (
                [string[]] $ProjectNames
            )
            foreach ($projectName in $ProjectNames) {
                $repos = Get-Repos -ProjectName $projectName
                Clone-ProjectRepos -ProjectName $projectName -Repos $repos
            }
        }

        function Get-Repos {
            param (
                [string] $ProjectName
            )
            return az repos list --organization $OrganizationUrl --project $ProjectName | ConvertFrom-Json
        }

        function Clone-ProjectRepos {
            param (
                [string] $ProjectName,
                [PSCustomObject] $Repos
            )
            $projectPath = New-ProjectFolder -ProjectName $ProjectName
            foreach ($repo in $Repos) {
                if(-not (Test-Path -Path $repo.name -PathType Container)) {
                    $repoPath = "${projectPath}\$($repo.Name)"
                    Write-Host -Object "Cloning $($repo.Name) to ${repoPath}" -ForegroundColor Yellow
                    git clone --mirror $repo.webUrl $repoPath
                }
            }
        }

        function New-ProjectFolder {
            param (
                [string] $ProjectName
            )
            return New-Item -Path ".\${ProjectName}" -ItemType Directory -Force | Select-Object -ExpandProperty FullName
        }
    }

    process {
        if ($null -eq (az extension list --query '[?name == ''azure-devops''].name' -o tsv)) {
            az extension add --name 'azure-devops' | Out-Null
        }

        $accountInfo = az account show 2>&1
        try {
            $accountInfo = $accountInfo | ConvertFrom-Json -ErrorAction Stop
        } catch {
            az login --allow-no-subscriptions
        }

        $projectNames = Get-ProjectNames
        Clone-Projects -ProjectNames $projectNames
    }

    end {
        Write-Host -Object "Cloning $($repo.Name) to ${repoPath}" -ForegroundColor Green
    }
}

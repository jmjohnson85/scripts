# Windows PowerShell Profile

## Example
```ps1
> vim $PROFILE.CurrentUserAllHosts
```
```ps1
. $env:USERPROFILE\source\repos.personal\jmjohnson85\scripts\ps1\profile.ps1
. $env:USERPROFILE\source\repos\scripts\ps1\profile.ps1
oh-my-posh init pwsh --config 'https://raw.githubusercontent.com/jeremyj563/wsl-setup/master/files/oh-my-posh/themes/jj.omp.json' | Invoke-Expression
```
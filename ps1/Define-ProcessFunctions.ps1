function Wait-ProcessStart {
	param (
		[string] $Name,
		[int] $Seconds = 5
	)
	do { 
		Start-Sleep -Seconds $Seconds
	} until ( 
		Get-Process -Name $Name -ErrorAction SilentlyContinue
	)
}

function Start-ProcessMinimized {
	param (
		[string] $ProcessName,
		[string] $FilePath,
		[string[]] $ArgumentList
	)
	$existingProcess = Get-Process -Name $ProcessName -ErrorAction SilentlyContinue
	if ($existingProcess -eq $null) {
		$process = Start-Process -PassThru -FilePath $FilePath -ArgumentList $ArgumentList -WindowStyle Minimized -ErrorAction SilentlyContinue
		Wait-ProcessStart -Name $ProcessName
		Set-WindowState -InputObject $process -State FORCEMINIMIZE -ErrorAction SilentlyContinue
	} else {
		Write-Warning -Message "Process already running: ${ProcessName}"
	}
}
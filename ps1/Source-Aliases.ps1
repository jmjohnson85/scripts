<##> Write-Host -Object "Loading ${PSCommandPath}..."

# USER DEFINED ALIASES
New-Alias -Name c  -Value 'Clear-Host'
New-Alias -Name k  -Value 'kubectl'
New-Alias -Name l  -Value 'ls.exe'


# kubectl autocomplete
kubectl completion powershell | Out-String | Invoke-Expression
Register-ArgumentCompleter -CommandName k -ScriptBlock $__kubectlCompleterBlock

# "aliases" that accept params are just functions...
function New-Function ([string] $Name, [string] $Value) {
	New-Item -Path function:\ -Name "global:$Name" -Value "${Value}" -Force -Option AllScope | Out-Null
}

# helps to quickly show the function definition when working in the terminal
function Get-Function ([string] $Name, [switch] $ShowDefinition) {
  $cmd = Get-Command -Name $Name
  if ($ShowDefinition) { return $cmd.Definition } else { return $cmd }
}

# USER DEFINED "ALIAS" FUNCTIONS

# general
New-Function -Name ll    -Value 'ls.exe -la'

# oh-my-posh
New-Function -Name p     -Value 'foreach ($segment in @("command", "kubectl", "os", "path", "time", "go", "python")) { oh-my-posh toggle $segment }'

# aws
New-Function -Name api   -Value 'aws sts get-caller-identity'
New-Function -Name apls  -Value 'aws configure list-profiles'
New-Function -Name apu   -Value 'Set-EnvironmentVariable -Name AWS_PROFILE -Value $args[0]'
New-Function -Name apc   -Value 'Get-EnvironmentVariable -Name AWS_PROFILE'

# kubectl
New-Function -Name ki    -Value 'kubectl cluster-info'
New-Function -Name kcc   -Value 'kubectl config current-context'
New-Function -Name kcg   -Value 'kubectl config get-contexts $args'
New-Function -Name kcu   -Value 'kubectl config use-context $args'
New-Function -Name kctx  -Value 'kubectl config use-context $(kubectl config get-contexts -o name | fzf --height=10 --prompt="Kubernetes Context> ")'
New-Function -Name kex   -Value 'kubectl exec -i -t $args'
New-Function -Name klo   -Value 'kubectl logs -f $args'
New-Function -Name klop  -Value 'kubectl logs -f -p $args'
New-Function -Name kd    -Value 'kubectl describe $args'
New-Function -Name kdno  -Value 'kubectl describe node $args'
New-Function -Name kdns  -Value 'kubectl describe namespace $args'
New-Function -Name kdpo  -Value 'kubectl describe pod $args'
New-Function -Name kdsvc -Value 'kubectl describe service $args'
New-Function -Name kding -Value 'kubectl describe ingress $args'
New-Function -Name kddep -Value 'kubectl describe deployment $args'
New-Function -Name kdsts -Value 'kubectl describe statefulset $args'
New-Function -Name kdds  -Value 'kubectl describe daemonset $args'
New-Function -Name kdsec -Value 'kubectl describe secret $args'
New-Function -Name kdcm  -Value 'kubectl describe configmap $args'
New-Function -Name kdr   -Value 'kubectl describe role $args'
New-Function -Name kdrb  -Value 'kubectl describe rolebinding $args'
New-Function -Name kdsa  -Value 'kubectl describe serviceaccount $args'
New-Function -Name kdpv  -Value 'kubectl describe persistentvolume $args'
New-Function -Name kdpvc -Value 'kubectl describe persistentvolumeclaim $args'
New-Function -Name kg    -Value 'kubectl get $args'
New-Function -Name kgno  -Value 'kubectl get node $args'
New-Function -Name kgns  -Value 'kubectl get namespace $args'
New-Function -Name kgpo  -Value 'kubectl get pod $args'
New-Function -Name kgsvc -Value 'kubectl get service $args'
New-Function -Name kging -Value 'kubectl get ingress $args'
New-Function -Name kgdep -Value 'kubectl get deployment $args'
New-Function -Name kgsts -Value 'kubectl get statefulset $args'
New-Function -Name kgds  -Value 'kubectl get daemonset $args'
New-Function -Name kgsec -Value 'kubectl get secret $args'
New-Function -Name kgcm  -Value 'kubectl get configmap $args'
New-Function -Name kgr   -Value 'kubectl get role $args'
New-Function -Name kgrb  -Value 'kubectl get rolebinding $args'
New-Function -Name kgsa  -Value 'kubectl get serviceaccount $args'
New-Function -Name kgpv  -Value 'kubectl get persistentvolume $args'
New-Function -Name kgpvc -Value 'kubectl get persistentvolumeclaim $args'

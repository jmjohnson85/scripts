﻿<#   
.SYNOPSIS   
Function that allows a domain admin to set any domain user account password quickly from the command line

.DESCRIPTION 
Requires the Set-ADAccountPassword Cmdlet included in the ActiveDirectory PowerShell Module

.PARAMETER UserName
[string] The username to set the password for

.NOTES   
Name: Set-DomainUserPassword.ps1
Author: Jeremy Johnson
DateCreated: 07-27-2018
DateUpdated: 08-17-2021
Version: 1.0.3

.EXAMPLE
	PS > . .\Set-DomainUserPassword.ps1

.EXAMPLE
    PS > Set-DomainUserPassword -UserName [user-name]

.EXAMPLE
    PS > Set-DomainUserPassword -u [user]
#>

#Requires -Version 7.0

function Set-DomainUserPassword {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]
        [Alias('u')]
        [string] $UserName
    )

    begin {
        function Get-Password {
            do {       
                Write-Host -Object ("`nEnter new password for user: {0}`n" -f $UserName) -ForegroundColor Green
        
                [securestring] $entry1 = Read-Host -Prompt "new password" -AsSecureString
                [securestring] $entry2 = Read-Host -Prompt "new password again" -AsSecureString
            } while (!(Compare-PasswordEntries -Entry1 $entry1 -Entry2 $entry2))
        
            return $entry1
        }
        function Compare-PasswordEntries {
            param (
                [securestring] $Entry1,
                [securestring] $Entry2
            )
        
            [string] $plainText1 = ConvertFrom-SecureString -SecureString $Entry1 -AsPlainText
            [string] $plainText2 = ConvertFrom-SecureString -SecureString $Entry2 -AsPlainText
        
            if ($plainText1 -ceq $plainText2) {
                return $true
            } else {
                Write-Host -Object "`nPasswords are different"
                return $false
            }
        }
    }

    process {
        [securestring] $password = Get-Password
        Set-ADAccountPassword -Identity $UserName -Reset -NewPassword $password
    }

    end {
    }
}

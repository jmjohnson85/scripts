function Start-ProcessSync {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]
        [Alias("p")]
        [string] $FilePath,

        [Parameter(Mandatory=$false)]
        [Alias("a")]
        [string] $ArgumentList = "",

        [Parameter(Mandatory=$false, ValueFromPipeline=$true)]
        [Alias("i")]
        [string] $StandardInput
    )

    begin {
        function New-ProcessStartInfo {
            param (
                [string] $FilePath,
                [string] $ArgumentList
            )
            $startInfo = [System.Diagnostics.ProcessStartInfo]::new()
            $startInfo.FileName = $FilePath
            $startInfo.RedirectStandardError = $true
            $startInfo.RedirectStandardOutput = $true
            $startInfo.RedirectStandardInput = $true
            $startInfo.UseShellExecute = $false
            $startInfo.Arguments = $ArgumentList
            return $startInfo
        }
        function Start-NewProcess {
            param (
                [System.Diagnostics.ProcessStartInfo] $StartInfo
            )
            $process = [System.Diagnostics.Process]::new()
            $process.StartInfo = $StartInfo
            $process.Start() | Out-Null
            $process.StandardInput.Write($StandardInput)
            $process.StandardInput.Close()
            return $process
        }
    }

    process {
        $startInfo = New-ProcessStartInfo -FilePath $FilePath -ArgumentList $ArgumentList
        $script:process = Start-NewProcess -StartInfo $startInfo
        $script:process.WaitForExit()
    }

    end {
        $exitCode = $script:process.ExitCode
        $standardOutput = $script:process.StandardOutput.ReadToEnd()
        $standardError = $script:process.StandardError.ReadToEnd()
        return [PSCustomObject]@{
            ExitCode = $exitCode
            StandardInput = $StandardInput
            StandardOutput = $standardOutput
            StandardError = $standardError
        }
    }
}

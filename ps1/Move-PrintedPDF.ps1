<#   
.SYNOPSIS
Function that moves a virtually printed PDF file from it's temporary path to a permanent one
    
.DESCRIPTION 
Gets the most recent instance of Event ID 307 to determine the default input path of the PDF and to generate the new filename

.PARAMETER Path
[IO.FileInfo] The fully qualified input path of where to find the PDF file to be moved

.PARAMETER Destination
[IO.FileInfo] The output directory of where to move the PDF file to

.PARAMETER NewName
[string] The new filename to give the moved PDF

.INPUTS
None. You cannot pipe objects to Move-PrintedPDF

.OUTPUTS
[string] The new path to the moved PDF file

.NOTES   
Name: Move-PrintedPDF.ps1
Author: Jeremy Johnson
Date Created: 5-25-2021
Date Updated: 6-11-2021
Version: 1.0.4

.EXAMPLE
    PS > . .\Move-PrintedPDF.ps1

.EXAMPLE
    PS > Move-PrintedPDF -Path '[input\path\original-name.pdf]' -Destination '[output\path\]' -NewName 'new-name.pdf'

.EXAMPLE
    PS > Move-PrintedPDF -p [in\path\orig.pdf] -d [out\path\] -n 'new.pdf'

.EXAMPLE
    PS > Move-PrintedPDF
#>

function Move-PrintedPDF {
    [CmdletBinding()]
    param (
        [ValidateScript({ if (-Not ($_ | Test-Path -PathType Leaf)) { throw "'$_' does not exist." } return $true })]
        [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
        [Alias('p')]
        [IO.FileInfo] $Path = $null,

        [ValidateScript({ if (-Not ($_ | Test-Path -PathType Container)) { throw "'$_' does not exist." } return $true })]
        [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
        [Alias('d')]
        [IO.FileInfo] $Destination = $null,

        [Parameter(Mandatory=$false, ValueFromPipeline=$false)]
        [Alias('n')]
        [string] $NewName = $null
    )

    begin {
        function Get-EventInstance {
            param (
                [string] $LogName,
                [string] $ID
            )
            $eventInstance = Get-WinEvent -FilterHashTable @{LogName=$LogName; ID=$ID} |
                Sort-Object -Property TimeCreated -Descending |
                Select-Object -First 1
            return $eventInstance
        }
        function Get-NewName {
            param (
                [System.Diagnostics.Eventing.Reader.EventLogRecord] $EventInstance
            )
            $userName = $EventInstance.Properties[2].Value
            $recordId = $EventInstance.RecordId
            $printerName = $EventInstance.Properties[4].Value
            $dateTime = Get-Date -Format FileDateTimeUniversal
            $newName = "$userName-$recordId-$printerName-$dateTime.pdf"
            return $newName
        }
    }

    process {
        $eventInstance = Get-EventInstance -LogName 'Microsoft-Windows-PrintService/Operational' -ID 307
        
        $path = if ([string]::IsNullOrEmpty($Path)) { $eventInstance.Properties[5].Value } else { $Path }
        $destination = if ([string]::IsNullOrEmpty($Destination)) { (Get-Item -Path $Path).DirectoryName } else { $Destination }
        $newName = if ([string]::IsNullOrEmpty($NewName)) { Get-NewName -EventInstance $eventInstance } else { $NewName }

        $pdfFile = Rename-Item -Path $path -NewName $newName -PassThru
        $pdfFile = Move-Item -Path $pdfFile.FullName -Destination $destination -PassThru
        return $pdfFile.FullName
    }

    end {
    }
}

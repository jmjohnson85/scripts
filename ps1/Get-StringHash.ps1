<#
.SYNOPSIS
Computes the hash of a string value
 
.DESCRIPTION
Uses the built-in cmdlet Get-FileHash as documented in the link below

.LINK
https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.utility/get-filehash?view=powershell-7.2#example-4-compute-the-hash-of-a-string
 
.PARAMETER Value
[string] The string value to compute the hash from
 
.PARAMETER Algorithm
[string] Specifies the cryptographic hash function to use for computing the hash value.
         The acceptable values are: 'SHA1', 'SHA256', 'SHA384', 'SHA512', 'MD5'

.INPUTS
[string] Value can be piped directly to Get-StringHash

.OUTPUTS
[string] The computed hash of the provided string value

.NOTES   
Name: Get-StringHash.ps1
Author: Jeremy Johnson
Date Created: 4-5-2022
Date Updated: 4-5-2022
Version: 1.0.0

.EXAMPLE
    PS > . .\Get-StringHash.ps1

.EXAMPLE
    PS > Get-StringHash -Value '[foo]' -Algorithm 'SHA1'

.EXAMPLE
    PS > Get-StringHash -v '[bar]' -a 'MD5' 

.EXAMPLE
    PS > 'foobar' | Get-StringHash -Algorithm 'SHA256'
#>

function Get-StringHash {
    [CmdletBinding()]
    [OutputType([string])]
    param (
        [Parameter(ValueFromPipeline, Mandatory = $true)]
        [Alias('v')]
        [string] $Value,

        [Parameter(Mandatory = $true)]
        [ValidateSet('SHA1', 'SHA256', 'SHA384', 'SHA512', 'MD5')]
        [Alias('a')]
        [string] $Algorithm
    )

    begin {
        $local:stringAsStream = [System.IO.MemoryStream]::new()
        $local:writer = [System.IO.StreamWriter]::new($stringAsStream)
    }

    process {
        $local:writer.write($Value)
        $local:writer.Flush()
        $local:stringAsStream.Position = 0
        $local:hash = Get-FileHash `
            -InputStream $local:stringAsStream `
            -Algorithm $Algorithm `
        | Select-Object -Property Hash
    }

    end {
        return $local:hash
    }
}
<#   
.SYNOPSIS
Function that sends a test email
    
.DESCRIPTION 
Uses Net.Mail.SmtpClient to generate and send an email to the specified address

.PARAMETER ToAddress
[string] The destination email address

.PARAMETER FromAddress
[string] The email address to use in the 'From:' field

.PARAMETER Subject
[string] The value to use in the 'Subject:' field

.PARAMETER Body
[string] The body of the email message

.PARAMETER SmtpHost
[string] The DNS hostname of the outgoing SMTP server

.PARAMETER SmtpPort
[int] The port number that the SMTP server is listening on

.NOTES   
Name: Send-TestEmail.ps1
Author: Jeremy Johnson
Date Created: 10-4-2021
Date Updated: 5-6-2024
Version: 1.0.2

.EXAMPLE
    PS > . .\Send-TestEmail.ps1

.EXAMPLE
    PS > Send-TestEmail -ToAddress '[user@domain.abc]' -SmtpHost mail.davenportiowa.com -SmtpPort 25

.EXAMPLE
    PS > Send-TestEmail -t [user@domain.abc] -h ex1 -p 25

.EXAMPLE
    PS> Send-TestEmail -ToAddress modernclassicgamer@gmail.com -FromAddress testing@jerlyd.com -SmtpHost smtp.gmail.com -SmtpPort 587 -UserName jerlyd8115@gmail.com -Password mqpqhsopgbrzqdnr
#>

function Send-TestEmail {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$false)]
        [Alias('t')]
        [string] $ToAddress = $null,

        [Parameter(Mandatory=$false)]
        [Alias('f')]
        [string] $FromAddress = 'noreply@davenportiowa.com',

        [Parameter(Mandatory=$false)]
        [Alias('s')]
        [string] $Subject = 'Test Email',

        [Parameter(Mandatory=$false)]
        [Alias('b')]
        [string] $Body = 'Thank you and have a nice day!',

        [Parameter(Mandatory=$false)]
        [Alias('h')]
        [string] $SmtpHost = 'mail.davenportiowa.com',

        [Parameter(Mandatory=$false)]
        [Alias('p')]
        [int] $SmtpPort = 25,

        [Parameter(Mandatory=$false)]
        [Alias('m')]
        $DeliveryMethod = [Net.Mail.SmtpDeliveryMethod]::Network,

        [Parameter(Mandatory=$false)]
        [Alias('user')]
        [string] $UserName,

        [Parameter(Mandatory=$false)]
        [Alias('pass')]
        [string] $Password,

        [Parameter(Mandatory=$false)]
        [Alias('i')]
        [bool] $IsBodyHtml = $true,

        [Parameter(Mandatory=$false)]
        [Alias('e')]
        [bool] $EnableSsl = $true,

        [Parameter(Mandatory=$false)]
        [Alias('d')]
        [bool] $UseDefaultCredentials = $false
    )

    begin {
        [System.Net.ServicePointManager]::ServerCertificateValidationCallback = {
            return $true # Disable HTTPS certificate validation
        }
        function New-NetworkCredential {
            if ($UserName -ne $null -and $Password -ne $null) {
                $credential = New-Object -TypeName Net.NetworkCredential($UserName, $Password)
                return $credential
            }
        }
        function New-SmtpClient {
            $client = New-Object -TypeName Net.Mail.SmtpClient($SmtpHost, $SmtpPort)
            $client.DeliveryMethod = $DeliveryMethod
            $client.EnableSsl = $EnableSsl
            $client.UseDefaultCredentials = $UseDefaultCredentials
            $client.Credentials = New-NetworkCredential
            return $client
        }
        function New-MailMessage {
            $message = New-Object -TypeName Net.Mail.MailMessage
            $message.From = $FromAddress
            $message.To.Add($ToAddress)
            $message.IsBodyHtml = $IsBodyHtml
            $message.Subject = $Subject
            $message.Body = $Body
            return $message
        }
    }

    process {
        $script:client = New-SmtpClient
        $script:message = New-MailMessage
    }

    end {
        $script:client.Send($script:message)
    }
}

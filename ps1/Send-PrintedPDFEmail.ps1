<#   
.SYNOPSIS
Function that sends a virtually printed PDF to the user via email
    
.DESCRIPTION 
Generates an email message, attaches the specified PDF file, then sends the email to the specified address

.PARAMETER AttachmentPath
[IO.FileInfo] The fully qualified input path of where to find the PDF file to be sent

.PARAMETER ToAddress
[string] The destination email address

.PARAMETER FromAddress
[string] The email address to use in the 'From:' field

.PARAMETER Subject
[string] The value to use in the 'Subject:' field

.PARAMETER Body
[string] The body of the email message

.PARAMETER SmtpHost
[string] The DNS hostname of the outgoing SMTP server

.PARAMETER SmtpPort
[int] The port number that the SMTP server is listening on

.INPUTS
[string] AttachmentPath can be piped to Send-PrintedPDFEmail

.OUTPUTS
[string] A string with the username, email address, and path to the sent PDF

.NOTES   
Name: Send-PrintedPDFEmail.ps1
Author: Jeremy Johnson
Date Created: 5-26-2021
Date Updated: 10-4-2021
Version: 1.0.5

.EXAMPLE
    PS > . .\Send-PrintedPDFEmail.ps1

.EXAMPLE
    PS > Send-PrintedPDFEmail -AttachmentPath '[input\path\file.pdf]' -ToAddress '[user@domain.abc]' -SmtpHost ex1.davenportiowa.com -SmtpPort 25

.EXAMPLE
    PS > Send-PrintedPDFEmail -a [in\path\file.pdf] -t [user@domain.abc] -h ex1 -p 25
#>

function Send-PrintedPDFEmail {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [Alias('a')]
        [IO.FileInfo] $AttachmentPath,

        [Parameter(Mandatory=$false)]
        [Alias('t')]
        [string] $ToAddress = $null,

        [Parameter(Mandatory=$false)]
        [Alias('f')]
        [string] $FromAddress = 'noreply@davenportiowa.com',

        [Parameter(Mandatory=$false)]
        [Alias('s')]
        [string] $Subject = 'Your Printed PDF Report',

        [Parameter(Mandatory=$false)]
        [Alias('b')]
        [string] $Body = 'Thank you and have a nice day!',

        [Parameter(Mandatory=$false)]
        [Alias('h')]
        [string] $SmtpHost = 'mail.davenportiowa.com',

        [Parameter(Mandatory=$false)]
        [Alias('p')]
        [int] $SmtpPort = 25
    )

    begin {
        [System.Net.ServicePointManager]::ServerCertificateValidationCallback = {
            return $true # Disable HTTPS certificate validation
        }
    }

    process {
        function Get-LdapUser {
            param (
                [string] $UserName
            )
            $ds_user = Get-WmiObject -Namespace "ROOT\directory\LDAP" -Class "ds_user" -Filter "DS_sAMAccountName='$UserName'"
            return $ds_user
        }
        function Get-ToAddress {
            param (
                [string] $UserName
            )
            if ([string]::IsNullOrEmpty($ToAddress)) {
                $ldapUser = Get-LdapUser -UserName $UserName
                return $ldapUser.DS_mail
            } else {
                return $ToAddress
            }
        }
        function New-SmtpClient {
            $client = New-Object -TypeName Net.Mail.SmtpClient($SmtpHost, $SmtpPort)
            $client.EnableSsl = $true
            $client.UseDefaultCredentials = $false
            return $client
        }
        function New-MailMessage {
            param (
                [string] $ToAddress
            )
            $message = New-Object -TypeName Net.Mail.MailMessage
            $message.From = $FromAddress
            $message.To.Add($ToAddress)
            $message.IsBodyHtml = $true
            $message.Subject = $Subject
            $attachment = New-Object -TypeName Net.Mail.Attachment($AttachmentPath)
            $message.Attachments.Add($attachment)
            $message.Body = $Body
            return $message
        }
    }

    end {
        $userName = $AttachmentPath.BaseName.Split('-')[0]
        $client = New-SmtpClient
        $toAddress = Get-ToAddress -UserName $userName
        $message = New-MailMessage -ToAddress $toAddress
        $client.Send($message)
        $result = "$userName - $toAddress - $AttachmentPath"
        return $result
    }
}

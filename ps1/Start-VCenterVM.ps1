<#   
.SYNOPSIS
Function that starts a virtual machine in vCenter
    
.DESCRIPTION 
Uses VMWare PowerCLI to connect to vCenter and starts the desired virtual machine

.PARAMETER User
[string] The username to use when connecting to the vCenter server

.PARAMETER Password
[string] The password to use when connecting to the vCenter server

.PARAMETER ServerAddress
[string] The hostname or IP address of the vCenter server to connect to

.PARAMETER DataCenterName
[string] The name of the data center to store the new virtual machine

.PARAMETER VMName
[string] The name to assign to the newly create virtual machine

.OUTPUTS
Zero or more powered-on VirtualMachine objects

.NOTES   
Name: Start-vCenterVM.ps1
Author: Jeremy Johnson
Date Created: 1-15-2019
Date Updated: 3-31-2021
Version: 1.0.2

.LINK
https://www.jmjohnson85.com

.EXAMPLE
    PS > . .\Start-vCenterVM.ps1

.EXAMPLE
    PS > Start-vCenterVM -User "[username]" -Password "[password]" -ServerAddress "[vcenter-host]" -DataCenterName "[datacenter-name]" -VMName "[vm-name]"

.EXAMPLE
    PS > Start-vCenterVM -User jeremy.m.johnson -Password [super-secret-password] -ServerAddress srv-vcs02 -DataCenterName COD-Server -VMName 'test-vm-ubuntu-20.04'
#>

function Start-vCenterVM {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]
        [Alias('u')]
        [string] $User,

        [Parameter(Mandatory=$true)]
        [Alias('p')]
        [string] $Password,

        [Parameter(Mandatory=$true)]
        [Alias('s')]
        [string] $ServerAddress,

        [Parameter(Mandatory=$true)]
        [Alias('d')]
        [string] $DataCenterName,

        [Parameter(Mandatory=$true)]
        [Alias('v')]
        [string] $VMName
    )

    begin {
        Set-PowerCLIConfiguration -Scope Session -ParticipateInCeip $false -DefaultVIServerMode Single -InvalidCertificateAction Ignore -Confirm:$false | Out-Null
    }
    
    process {
        $server = Connect-VIServer -Server $ServerAddress -User $User -Password $Password
        $datacenter = Get-DataCenter -Server $server -Name $DataCenterName
        $vm = Get-VM -Server $server -Location $datacenter -Name $VMName
        $result = Start-VM -VM $vm
        return $result
    }
    
    end {

    }
}
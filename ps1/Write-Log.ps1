<#   
.SYNOPSIS
Function that writes (appends) a message to a log file along with a log level and timestamp
    
.DESCRIPTION 
Uses the Add-Content cmdlet to write the message to the log file at the specified path

.PARAMETER Message
[string] The message to be written to the log

.PARAMETER Path
[string] The path of the log file to be written to

.PARAMETER Level
[string] The optional log level of the message to be written

.INPUTS
[string] Message can be piped to Write-Log

.OUTPUTS
None

.NOTES   
Name: Write-Log.ps1
Original Author: Ben Newton
Modified By: Jeremy Johnson
Date Created: 6-10-2021
Date Updated: 6-10-2021
Version: 1.0.0

.LINK
https://stackoverflow.com/a/38738942/8302472

.EXAMPLE
    PS > . .\Write-Log.ps1

.EXAMPLE
    PS > Write-Log -Message [message] -Path [path/to/file.log] -Level Error

.EXAMPLE
    PS > Write-Log -m [msg] -p [file.log]
#>

function Write-Log {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true, ValueFromPipeline=$true)]
        [Alias('m')]
        [string] $Message,

        [Parameter(Mandatory=$true)]
        [Alias('p')]
        [string] $Path,

        [Parameter(Mandatory=$false)]
        [Alias('l')]
        [ValidateSet("INFO","WARN","ERROR","FATAL","DEBUG")]
        [string] $Level = "INFO"
    )

    $timeStamp = (Get-Date).ToString("yyyy/MM/dd HH:mm:ss")
    $value = "$timeStamp $Level $Message"

    if ($Path) {
        Add-Content -Path $Path -Value $value
    } else {
        Write-Output -InputObject $value
    }
}

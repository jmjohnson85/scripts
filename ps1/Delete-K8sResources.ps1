function Delete-K8sResources {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]
        [Alias('s')]
        [string] $SimpleMatch,

        [Parameter(Mandatory=$false)]
        [Alias('n')]
        [switch] $NoDryRun = $true
    )

    begin {
        function Get-TypeNames {
            $typeNames = kubectl api-resources --verbs=delete --output=name
            return $typeNames
        }
        function Get-ResourceNames {
            param (
                [string] $Type
            )
            $resourceNames = kubectl get $Type --no-headers -o custom-columns=":metadata.name"
            $matchedNames = $resourceNames | Select-String -SimpleMatch $SimpleMatch
            return $matchedNames
        }
        function Delete-Resources {
            param (
                [string] $Type,
                [string[]] $Resources
            )
            $resources = $Resources -join ' '
            if (! [string]::IsNullorWhitespace($resources)) {
                $extraArgs = if (! $NoDryRun) {'--dry-run=client'}
                $messageColor = if ($NoDryRun) {'Yellow'} else {'Green'}
                $message = "Executing command: kubectl delete $Type $resources $extraArgs"
                Write-Host -Object "`n${message}`n" -ForegroundColor $messageColor
                kubectl delete $Type $resources $extraArgs
            }
        }
    }

    process {
        $types = Get-TypeNames
        foreach ($type in $types) {
            $resources = Get-ResourceNames -Type $type
            Delete-Resources -Type $type -Resources $resources
        }
    }

    end {
    }
}
# choose a default path where the PDF is saved
$pdfPath = "$env:temp\VirtualPDF.pdf"

# check whether port exists
if (-Not (Get-PrinterPort -Name $pdfPath -ErrorAction SilentlyContinue)) {
    Add-PrinterPort -Name $pdfPath 
}

# add printer
Add-Printer -DriverName "Microsoft Print to PDF" -Name VirtualPDF -PortName $pdfPath
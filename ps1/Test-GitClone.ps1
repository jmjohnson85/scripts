<#   
.SYNOPSIS
Function that runs a 'git clone' command on an infinite loop
    
.DESCRIPTION 
Infinitely attempts to clone a git repo from the provided URL. Pauses a specified number of seconds between each clone attempt.

.PARAMETER Url
[string] The URL to attempt the 'git clone' from

.PARAMETER TargetPath
[string] Optional target path of where to clone the repo. Defaults to "$env:TEMP\Test-GitClone".

.PARAMETER Seconds
[int] The number of seconds to pause after each clone attempt

.INPUTS
None. You cannot pipe objects to Test-GitClone

.OUTPUTS
The current date/time and the 'git clone' result of each attempt

.NOTES   
Name: Test-GitClone.ps1
Author: Jeremy Johnson
Date Created: 3-18-2022
Date Updated: 3-18-2022
Version: 1.0.0

.EXAMPLE
    PS > . .\Test-GitClone.ps1

.EXAMPLE
    PS > Test-GitClone -Url 'https://github.com/ijin/clone-test.git' -TargetPath 'clone/repo/here' -Seconds 5

.EXAMPLE
    PS > Test-GitClone -Url https://cod-jmjohnson@gitlab.com/departments/finance-ar/functions/example.git -Seconds 5 | tee log.txt

.EXAMPLE
    PS > Test-GitClone -Url '[source-url]' -Seconds [num-seconds] | Tee-Object -FilePath '.\path\to\log.file'

.EXAMPLE
    PS > Test-GitClone -u [url] -s [num] | tee log.txt
#>

function Test-GitClone {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]
        [Alias('u')]
        [string] $Url,

        [Parameter(Mandatory=$false)]
        [Alias('t')]
        [string] $TargetPath = "$env:TEMP\Test-GitClone",

        [Parameter(Mandatory=$true)]
        [Alias('s')]
        [int] $Seconds
    )

    process {
        while ($true) {
            Remove-Item -Path $TargetPath -Recurse -Force -ErrorAction Ignore
            Get-Date -DisplayHint DateTime -Verbose
            git clone $Url $TargetPath
            Write-Output -InputObject "`r"
            Start-Sleep -Seconds $Seconds
        }
    }
}

Add-Type -AssemblyName System.Windows.Forms

while ($true) {
  $position = [System.Windows.Forms.Cursor]::Position
  $x = ($position.X % 500) + 1
  $y = ($position.Y % 500) + 1
  [System.Windows.Forms.Cursor]::Position = [System.Drawing.Point]::new($x, $y)
  Start-Sleep -Seconds 30
}
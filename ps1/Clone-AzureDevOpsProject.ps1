<#   
.SYNOPSIS
Function that clones all git repositories in a given Azure DevOps Project
    
.DESCRIPTION 
Uses the Azure DevOps REST API to get a list of all repos in a project, then runs 'git clone' for each one

.PARAMETER ProjectName
[string] The name of the Azure DevOps project to clone

.PARAMETER UserName
[string] The Azure DevOps username to authenticate with (most likely your email)

.PARAMETER Token
[string] A Personal Access Token (PAT) with 'Read' access on 'Code' scope

.PARAMETER OrganizationUrl
[string] The URL of the organization hosted on Azure DevOps to be cloned

.NOTES   
Name: Clone-AzureDevOpsProject.ps1
Author: Jeremy Johnson
Date Created: 11-3-2021
Date Updated: 11-3-2021
Version: 1.0.0

.LINK
Reference: https://gist.github.com/Ryanman/4cf6cd7b540e96a02b32011bfd1027c3

.EXAMPLE
    PS > . .\Clone-AzureDevOpsProject.ps1

.EXAMPLE
    PS > Clone-AzureDevOpsProject -ProjectName [project-name] -UserName [user@example.com] -Token [xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx] -OrganizationUrl [organization-url]

.EXAMPLE
    PS > Clone-AzureDevOpsProject -p [project] -u [user] -t [xxxx] -o [org-url]

.EXAMPLE
    PS > Clone-AzureDevOpsProject -p 'IT-K8s-Deployment' -u 'jeremy.m.johnson@davenportiowa.com' -t 'xefwe2763xja37d7zcune4i5ova1vs3q43scfv4q5gn2wwows45d2' -o 'https://dev.azure.com/davenportiowa'
#>

# TODO: implementation; note use reference link above
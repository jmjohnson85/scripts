<#   
.SYNOPSIS
Function that gets virtual machine data from VMware VSphere and outputs it to a GridView
    
.DESCRIPTION 
Uses PowerCLI to login to the VSphere API. PowerCLI can be installed using the following commands:
    PS > Install-Module VMware.PowerCLI -Scope CurrentUser -AllowClobber -Force
    PS > Set-PowerCLIConfiguration -Scope User -ParticipateInCeip $false -Confirm:$false
    PS > Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false

.PARAMETER Server
[string] The hostname of the VSphere server to get virtual machine data from

.INPUTS
None. You cannot pipe objects to Out-VSphereGridView

.OUTPUTS
A GridView object containing virtual machine data

.NOTES   
Name: Out-VSphereGridView.ps1
Author: Jeremy Johnson
Date Created: 3-16-2022
Date Updated: 3-16-2022
Version: 1.0.1

.EXAMPLE
    PS > . .\Out-VSphereGridView.ps1

.EXAMPLE
    PS > Out-VSphereGridView -Server [vsphere-hostname]

.EXAMPLE
    PS > Out-VSphereGridView -s [host]
#>

#Requires -Version 7.0

function Out-VSphereGridView {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]
        [Alias('s')]
        [string] $Server
    )

    begin {
        Connect-VIServer -Server $Server -NotDefault
        class ReportedVM {
            [string] $Name
            [string] $Network
            [string] $IpAddress
            [string] $DnsConfig
        }
    }

    process {
        $reportedVMs = [System.Collections.Generic.List[ReportedVM]]::new()
        $vms = Get-View -ViewType VirtualMachine -Server $Server
        foreach ($vm in $vms){
            $reportedVM = [ReportedVM]::new()
            $reportedVM.Name = $vm.Name
            $reportedVM.Network = $vm.Guest.Net?[0].Network
            $reportedVM.IpAddress = $vm.Guest.Net?[0].IpAddress
            $reportedVM.DnsConfig = $vm.Guest.Net?[0].DnsConfig?[0].IpAddress
            $reportedVMs.Add($reportedVM) | Out-Null
        }
    }

    end {
        $reportedVMs | Out-GridView
    }
}

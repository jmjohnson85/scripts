<#   
.SYNOPSIS
Function to create a new virtual machine in vCenter from the specified template
    
.DESCRIPTION 
Uses VMWare PowerCLI to connect to vCenter and create a new instance of a template

.PARAMETER User
[string] The username to use when connecting to the vCenter server

.PARAMETER Password
[string] The password to use when connecting to the vCenter server

.PARAMETER ServerAddress
[string] The hostname or IP address of the vCenter server to connect to

.PARAMETER DataCenterName
[string] The name of the data center to store the new virtual machine

.PARAMETER ResourcePoolName
[string] The name of the resource pool to use

.PARAMETER TemplateName
[string] The name of the template to create a new instance of

.PARAMETER VMName
[string] The name to assign to the newly create virtual machine

.PARAMETER Location
[string] The name of the folder to create the virtual machine within

.INPUTS
None. You cannot pipe objects to New-vCenterVM

.OUTPUTS
[string] The ID of the newly created VM

.NOTES   
Name: New-vCenterVM.ps1
Author: Jeremy Johnson
Date Created: 1-15-2019
Date Updated: 3-31-2021
Version: 1.0.2

.LINK
https://www.jmjohnson85.com

.EXAMPLE
    PS > . .\New-vCenterVM.ps1

.EXAMPLE
    PS > New-vCenterVM -User "[username]" -Password "[password]" -ServerAddress "[vcenter-host]" -DataCenterName "[dataCenter-name]" -ResourcePoolName "[pool-name]" -TemplateName "[template-name]" -VMName "[vm-name]" -Location "[folder-name]"

.EXAMPLE
    PS > New-vCenterVM -User jeremy.m.johnson -Password [super-secret-password] -ServerAddress srv-vcs02 -DataCenterName COD-Server -ResourcePoolName Resources -TemplateName 'Ubuntu 20.04 LTS' -VMName 'test-vm-ubuntu-20.04' -Location 'VMs to be deleted'
#>

function New-vCenterVM {
    [CmdletBinding()]
    param (
        [Parameter(Mandatory=$true)]
        [Alias('u')]
        [string] $User,

        [Parameter(Mandatory=$true)]
        [Alias('p')]
        [string] $Password,

        [Parameter(Mandatory=$true)]
        [Alias('s')]
        [string] $ServerAddress,

        [Parameter(Mandatory=$true)]
        [Alias('d')]
        [string] $DataCenterName,

        [Parameter(Mandatory=$true)]
        [Alias('r')]
        [string] $ResourcePoolName,

        [Parameter(Mandatory=$true)]
        [Alias('t')]
        [string] $TemplateName,

        [Parameter(Mandatory=$true)]
        [Alias('v')]
        [string] $VMName,

        [Parameter(Mandatory=$true)]
        [Alias('l')]
        [string] $Location
    )

    begin {
        Set-PowerCLIConfiguration -Scope Session -ParticipateInCeip $false -DefaultVIServerMode Single -InvalidCertificateAction Ignore -Confirm:$false | Out-Null
    }

    process {
        $server = Connect-VIServer -Server $ServerAddress -User $User -Password $Password
        $dataCenter = Get-DataCenter -Server $server -Name $DataCenterName
        $resourcePool = Get-ResourcePool -Location $dataCenter -Name $ResourcePoolName
        $template = Get-Template -Location $dataCenter -Name $TemplateName
        $vm = New-VM -Name $VMName -Template $template -ResourcePool $resourcePool -Location $Location
        return $vm.Id
    }

    end {

    }
}
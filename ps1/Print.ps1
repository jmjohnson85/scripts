powershell.exe -command {
    function Out-PDFFile
    {
        param
        (
            $Path = "$env:temp\results.pdf",
    
            [Switch]
            $Open
        )
    
        # check to see whether the PDF printer was set up correctly
        $printerName = "PrintPDFUnattended"
        $printer = Get-Printer -Name $printerName -ErrorAction SilentlyContinue
        if (!$?)
        {
            Write-Warning "Printer $printerName does not exist." 
            Write-Warning "Make sure you have created this printer (see previous tips)!"
            return
        }
    
        # this is the file the print driver always prints to
        $TempPDF = $printer.PortName
    
        # is the printer set up correctly and the port name is the output file path?
        if ($TempPDF -notlike '?:\*')
        {
            Write-Warning "Printer $printerName is not set up correctly." 
            Write-Warning "Make sure you have created this printer as instructed (see previous tips)!"
            return
        }
    
        # make sure old print results are removed
        $exists = Test-Path -Path $TempPDF
        if ($exists) { Remove-Item -Path $TempPDF -Force }
    
        # send anything that is piped to this function to PDF
        $input | Out-Printer -Name $printerName
    
        # wait for the print job to be completed, then move file
        $ok = $false
        do { 
            Start-Sleep -Milliseconds 500 
            Write-Host '.' -NoNewline
                    
            $fileExists = Test-Path -Path $TempPDF
            if ($fileExists)
            {
                try
                {
                    Move-Item -Path $TempPDF -Destination $Path -Force -ea Stop
                    $ok = $true
                }
                catch
                {
                    # file is still in use, cannot move
                    # try again
                }
            }
        } until ( $ok )
        Write-Host
    
        # open file if requested
        if ($Open)
        {
            Invoke-Item -Path $Path
        }
    }

    Get-Content -Path 
}
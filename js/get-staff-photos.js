// $response = Invoke-WebRequest `
// -Uri 'https://app.salamanderlive.com/resourcemgrsvc/Responder/List' `
// -Method Post `
// -Body $body `
// -Headers @{
//     'Content-Length' = $body.Length;
//     'Host' = 'app.salamanderlive.com';
//     'Authorization' = 'Bearer 3645eedd-f028-4c22-968a-4c8fe917de4e'
// };
// $content = $response.Content | ConvertFrom-Json


const https = require('https')

function httpRequest(options, requestBody) {
    return new Promise ((resolve, reject) => {
        const request = https.request(options, response => {
            let responseBody = [];
            response.on('data', c => { responseBody.push(c.toString()) })
            response.on('end', () => { resolve(responseBody) });
            response.on('error', e => {debugger; reject(e) });
        });
        request.write(requestBody)
        request.end()
    })
}

function newOptions() {
    return {
        method: 'POST',
        host: 'app.salamanderlive.com',
        port: 443,
        path: '/resourcemgrsvc/Responder/List',
        headers: {
            authorization: 'Bearer 43c81403-d7fb-4b1c-b6d2-2b840a4ddbaf'
        }
    }
}

function newBody() {
    return JSON.stringify({
        TimeZoneId: 'Central Standard Time',
        Item: {
            Page: 1,
            RecordsPerPage: 1000,
            Sort: [],
            Filter: [],
            Columns: [
                'LastName',
                'FirstName',
                'IdentityCode',
                'Rank',
                'OrganizationName'
            ]
        },
        Save: false
    })
}

async function getUsers() {
    let options = newOptions()
    const body = newBody()
    const response = await httpRequest(options, body)
    return response
}

async function getUserIds() {
    const users = await getUsers()
    const ids = users.map(u => u.PK)
    return ids
}

async function main() {
    const ids = await getUserIds()
    debugger
}

main()
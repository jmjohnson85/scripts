# Windows Command Prompt AutoRun Scripts

- [Windows Command Prompt AutoRun Scripts](#windows-command-prompt-autorun-scripts)
  - [Synopsis](#synopsis)
  - [(Optional) Install `clink` and `oh-my-posh`](#optional-install-clink-and-oh-my-posh)
  - [Setup `cmd` AutoRun Registry Key](#setup-cmd-autorun-registry-key)
    - [Current User Only](#current-user-only)
    - [Local Machine](#local-machine)
  - [Install `cmd` AutoRun Entrypoint Script (`autorun.cmd`)](#install-cmd-autorun-entrypoint-script-autoruncmd)

## Synopsis

The batch scripts found in this directory are intended to be loaded from a Windows Command Prompt (`cmd`) autorun entrypoint such as `%USERPROFILE%\autorun.cmd`

## (Optional) Install `clink` and `oh-my-posh`

```bat
:: install clink
> winget install clink
or
> scoop install clink

:: install oh-my-posh
> choco install -y oh-my-posh

:: configure clink profile
> vim %LOCALAPPDATA%\clink\oh-my-posh.lua

load(io.popen('oh-my-posh init cmd --config "https://raw.githubusercontent.com/jeremyj563/wsl-setup/master/files/oh-my-posh/themes/jj.omp.json"'):read("*a"))()
```

## Setup `cmd` AutoRun Registry Key

### Current User Only

```ps1
native : PS > Set-ItemProperty -Path 'HKCU:\SOFTWARE\Microsoft\Command Processor\' -Name AutoRun -Value '%USERPROFILE%\autorun.cmd'
wow64  : PS > Set-ItemProperty -Path 'HKCU:\SOFTWARE\Wow6432Node\Microsoft\Command Processor\' -Name AutoRun -Value '%USERPROFILE%\autorun.cmd'
```
or
```bat
native : > reg add "HKCU\SOFTWARE\Microsoft\Command Processor" /v AutoRun /t REG_SZ /d "%USERPROFILE%\autorun.cmd"
wow64  : > reg add "HKCU\SOFTWARE\Wow6432Node\Microsoft\Command Processor" /v AutoRun /t REG_SZ /d "%USERPROFILE%\autorun.cmd"
```

### Local Machine

```ps1
native : Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Command Processor\' -Name AutoRun -Value '%USERPROFILE%\autorun.cmd'
wow64  : Set-ItemProperty -Path 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\Command Processor\' -Name AutoRun -Value '%USERPROFILE%\autorun.cmd'
```
or
```bat
native : > reg add "HKLM\SOFTWARE\Microsoft\Command Processor" /v AutoRun /t REG_SZ /d "%USERPROFILE%\autorun.cmd"
wow64  : > reg add "HKLM\SOFTWARE\Wow6432Node\Microsoft\Command Processor" /v AutoRun /t REG_SZ /d "%USERPROFILE%\autorun.cmd"
```

## Install `cmd` AutoRun Entrypoint Script (`autorun.cmd`)

```bat
:: %USERPROFILE%\autorun.cmd

@echo off

:: The Command Processor AutoRun script is configured in the Windows Registry:
:: native : Computer\HKEY_CURRENT_USER\SOFTWARE\Microsoft\Command Processor
:: wow64  : Computer\HKEY_CURRENT_USER\SOFTWARE\Wow6432Node\Microsoft\Command Processor
::
:: This key can be quickly created like so:
:: native : > reg add "HKCU\SOFTWARE\Microsoft\Command Processor" /v AutoRun /t REG_SZ /d "%USERPROFILE%\autorun.cmd"
:: wow64  : > reg add "HKCU\SOFTWARE\Wow6432Node\Microsoft\Command Processor" /v AutoRun /t REG_SZ /d "%USERPROFILE%\autorun.cmd"

:: include distributable init scripts here
call "%USERPROFILE%\source\repos\scripts\bat\autorun\init.cmd" 2> nul
call "%USERPROFILE%\source\repos.personal\jmjohnson85\scripts\bat\autorun\init.cmd" 2> nul
```

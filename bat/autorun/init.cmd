:: %USERPROFILE%\source\repos.personal\jmjohnson85\scripts\bat\autorun\init.cmd

@echo off

:: The Command Processor AutoRun script is configured in the Windows Registry:
:: native : Computer\HKEY_CURRENT_USER\SOFTWARE\Microsoft\Command Processor
:: wow64  : Computer\HKEY_CURRENT_USER\SOFTWARE\Wow6432Node\Microsoft\Command Processor
::
:: This key can be quickly created like so:
:: native : > reg add "HKCU\SOFTWARE\Microsoft\Command Processor" /v AutoRun /t REG_SZ /d "%USERPROFILE%\autorun.cmd"
:: wow64  : > reg add "HKCU\SOFTWARE\Wow6432Node\Microsoft\Command Processor" /v AutoRun /t REG_SZ /d "%USERPROFILE%\autorun.cmd"

:: include all scripts here
call "%USERPROFILE%\source\repos.personal\jmjohnson85\scripts\bat\autorun\clink.bat"
call "%USERPROFILE%\source\repos.personal\jmjohnson85\scripts\bat\autorun\doskeys.bat"
call "%USERPROFILE%\source\repos.personal\jmjohnson85\scripts\bat\autorun\environment.bat"
@echo off
REM run this to disable the prompt "Terminate batch job (Y/N)?":
REM clink set terminate_autoanswer 1
kubectl config get-contexts -o name | fzf --height=10 --prompt="Kubernetes Context> " > %TEMP%\kctx-fzf-output.txt
set /p KUBE_CURRENT_CONTEXT=<%TEMP%\kctx-fzf-output.txt
kubectl config use-context %KUBE_CURRENT_CONTEXT% > nul 2>&1
